<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'Web\HomeController@index')->name('home');
Route::get('home', 'Web\HomeController@index');

Route::group(['prefix' => 'ticket', 'as' => 'ticket::'], function () {
    Route::get('index', 'Web\TicketController@index')->name('index');
    Route::get('report', 'Web\TicketController@report')->name('report');
    Route::get('reprint/{id}', 'Web\TicketController@reprint')->name('reprint');
    Route::post('store', 'Web\TicketController@store')->name('store');
    Route::put('update/{id}', 'Web\TicketController@update')->name('update');
    Route::delete('delete/{id}', 'Web\TicketController@delete')->name('delete');
});

Route::group(['prefix' => 'operator', 'as' => 'operator::'], function () {
    Route::get('index', 'Web\OperatorController@index')->name('index');
    Route::post('store', 'Web\OperatorController@store')->name('store');
    Route::put('update/{id}', 'Web\OperatorController@update')->name('update');
    Route::delete('delete/{id}', 'Web\OperatorController@delete')->name('delete');
});

Route::group(['prefix' => 'enrollment', 'as' => 'enrollment::'], function () {
    Route::get('index', 'Web\EnrollmentController@index')->name('index');
    Route::post('store', 'Web\EnrollmentController@store')->name('store');
    Route::put('update/{id}', 'Web\EnrollmentController@update')->name('update');
    Route::delete('delete/{id}', 'Web\EnrollmentController@delete')->name('delete');
});

Route::group(['prefix' => 'product', 'as' => 'product::'], function () {
    Route::get('index', 'Web\ProductController@index')->name('index');
    Route::post('store', 'Web\ProductController@store')->name('store');
    Route::put('update/{id}', 'Web\ProductController@update')->name('update');
    Route::delete('delete/{id}', 'Web\ProductController@delete')->name('delete');
});

Route::group(['prefix' => 'receipt', 'as' => 'receipt::'], function () {
    Route::get('index', 'Web\ReceiptController@index')->name('index');
    Route::put('update', 'Web\ReceiptController@update')->name('update');
});