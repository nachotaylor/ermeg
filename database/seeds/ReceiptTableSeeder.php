<?php

use App\Repositories\Receipt\Receipt;
use Illuminate\Database\Seeder;

class ReceiptTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Receipt::create([
            'image_url' => '',
            'header' => "ERMEG S.A.\nES.CHAPADMALAL 7605\nTEL: 022-34642154\nCUIT: 30-70079711-7\n",
            'footer' => "",
            'printer' => "POS-58"
        ]);
    }
}
