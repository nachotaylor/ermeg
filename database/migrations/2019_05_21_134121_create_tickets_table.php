<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date');
            $table->integer('axis1');
            $table->integer('axis2');
            $table->integer('axis3');
            $table->integer('axis4');
            $table->integer('axis5');
            $table->integer('axis_total');
            $table->unsignedBigInteger('enrollment_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('operator_id');
            $table->foreign('enrollment_id')->references('id')->on('enrollments');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('operator_id')->references('id')->on('operators');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
