@extends('layout')

@section('content')
    <div class="table-responsive">
        <div class="col-xs-12 row">
            <div class="box-header">
                <h1 class="box-title">Configuración de impresión</h1>
            </div>

            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i>Error!</h4>
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="col-md-6">
                <div class="box box-body">
                    <div class="box-header with-border">
                        <div class="box-body">
                            {!! Form::model($receipt, ['route' => ['receipt::update'], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                            <div class="form-group">
                                {!! Form::label('printer','Nombre de la impresora',['class' => 'control-label']) !!}
                                {!! Form::text('printer',null,['class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('header','Cabecera',['class' => 'control-label']) !!}
                                {!! Form::textarea('header',null,['class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('footer','Pie de ticket',['class' => 'control-label']) !!}
                                {!! Form::textarea('footer',null,['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('image_url','Logo',['class' => 'control-label']) !!}
                                {!! Form::file('image_url',null,['class' => 'form-control', 'required']) !!}
                            </div>
                            {!! Form::submit('Modificar',['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h2>Logo</h2>
                @if($receipt->image_url)
                    <img src="{{$receipt->image_url}}"/>
                @endif
            </div>
        </div>
@stop