@extends('layout')

@section('content')
    <div class="table-responsive">
        <div class="col-xs-12 row">
            <div class="box-header">
                <h1 class="box-title">Reporte</h1>
            </div>

            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i>Éxito!</h4>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i>Error!</h4>
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="box with-border">
                <div class="box-body" style="overflow-x: auto">
                    <table id="table" class="table table-condensed table-hover dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <h2>Total suma ejes: {{$tickets['sum']}}</h2>
                        <tr>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Patente</th>
                            <th>Producto</th>
                            <th>Operador</th>
                            <th>Eje 1</th>
                            <th>Eje 2</th>
                            <th>Eje 3</th>
                            <th>Eje 4</th>
                            <th>Eje 5</th>
                            <th>Suma ejes</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets['tickets'] as $ticket)
                            <tr>
                                <td>{{\Carbon\Carbon::parse($ticket->date)->format('d M y')}}</td>
                                <td>{{\Carbon\Carbon::parse($ticket->date)->format('H:i')}}</td>
                                <td>{{$ticket->enrollment}}</td>
                                <td>{{$ticket->product}}</td>
                                <td>{{$ticket->name}}</td>
                                <td>{{$ticket->axis1}}</td>
                                <td>{{$ticket->axis2}}</td>
                                <td>{{$ticket->axis3}}</td>
                                <td>{{$ticket->axis4}}</td>
                                <td>{{$ticket->axis5}}</td>
                                <td>{{$ticket->axis_total}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>

            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#reporte"><i
                        class="fa fa-search"></i> Buscar</a>

            <div class="modal fade in" id="reporte">
                <div class="modal-dialog modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Buscar</h4>
                    </div>
                    {!! Form::open((['route' => 'ticket::report', 'method' => 'GET'])) !!}
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('from','Desde',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::date('from',null,['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('to','Hasta',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::date('to',null,['class' => 'form-control' ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('enrollment','Pantente',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('enrollment_id',$enrollments->pluck('enrollment', 'id'), null,['class' => 'form-control', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('product','Producto',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('product_id',$products->pluck('product', 'id'), null,['class' => 'form-control', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar
                        </button>
                        {!! Form::submit('Buscar',['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                dom: 'Bfrtip',
                language: {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'
                },
                'order': [[0, 'asc']] // Order by Date
            });
        });
    </script>
@stop