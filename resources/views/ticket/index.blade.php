@extends('layout')

@section('content')
    <div class="table-responsive">
        <div class="col-xs-12 row">
            <div class="box-header">
                <h1 class="box-title">Ticket</h1>
            </div>

            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i>Éxito!</h4>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i>Error!</h4>
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="box with-border">
                <div class="box-body" style="overflow-x: auto">
                    <table id="table" class="table table-condensed table-hover dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Patente</th>
                            <th>Producto</th>
                            <th>Operador</th>
                            <th>Eje 1</th>
                            <th>Eje 2</th>
                            <th>Eje 3</th>
                            <th>Eje 4</th>
                            <th>Eje 5</th>
                            <th>Suma ejes</th>
                            <th>Imprimir</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets as $ticket)
                            <tr>
                                <td>{{\Carbon\Carbon::parse($ticket->date)->format('d M y')}}</td>
                                <td>{{\Carbon\Carbon::parse($ticket->date)->format('H:i')}}</td>
                                <td>{{$ticket->enrollment}}</td>
                                <td>{{$ticket->product}}</td>
                                <td>{{$ticket->name}}</td>
                                <td>{{$ticket->axis1}}</td>
                                <td>{{$ticket->axis2}}</td>
                                <td>{{$ticket->axis3}}</td>
                                <td>{{$ticket->axis4}}</td>
                                <td>{{$ticket->axis5}}</td>
                                <td>{{$ticket->axis_total}}</td>
                                <td>
                                    {!! Form::open(['route' => ['ticket::reprint', $ticket->id], 'method' => 'GET']) !!}
                                    {!! Form::button('<i class="fa fa-print"></i>',['class' => 'btn btn-success','type'=>'submit']) !!}
                                    {!! Form::close() !!}
                                </td>
                                <td>
                                    <a class="btn btn-primary" data-toggle="modal"
                                       data-target="#edit{{ $ticket->id }}"><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modal-danger{{ $ticket->id }}"><i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                            </tr>

                            <div class="modal fade in" id="edit{{ $ticket->id }}">
                                <div class="modal-dialog modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title">Editar ticket</h4>
                                    </div>
                                    {!! Form::model($ticket, ['route' => ['ticket::update', $ticket], 'method' => 'PUT']) !!}
                                    <div class="modal-body box-body">
                                        <div class="form-group">
                                            {!! Form::label('date','Fecha',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::date('date',\Carbon\Carbon::parse($ticket->date),['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('hour','Hora',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::time('hour',\Carbon\Carbon::parse($ticket->date)->format('H:i'),['class' => 'form-control', 'required', ]) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('enrollment','Pantente',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::select('enrollment_id',$enrollments->pluck('enrollment', 'id'), null,['class' => 'form-control', 'required', 'placeholder'=>'Seleccione opción']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('product','Producto',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::select('product_id',$products->pluck('product', 'id'), null,['class' => 'form-control', 'required', 'placeholder'=>'Seleccione opción']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('operator','Operador',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::select('operator_id',$operators->pluck('name', 'id'), null,['class' => 'form-control', 'required', 'placeholder'=>'Seleccione opción']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('axis1','Eje 1',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::number('axis1',null,['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('axis2','Eje 2',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::number('axis2',null,['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('axis3','Eje 3',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::number('axis3',null,['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('axis4','Eje 4',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::number('axis4',null,['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('axis5','Eje 5',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::number('axis5',null,['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left"
                                                data-dismiss="modal">Cerrar
                                        </button>
                                        {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="modal modal-danger fade" id="modal-danger{{ $ticket->id }}"
                                 style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">Delete</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Desea eliminar el ticket?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left"
                                                    data-dismiss="modal">Cerrar
                                            </button>
                                            {!! Form::model($ticket, ['route' => ['ticket::delete', $ticket], 'method' => 'DELETE']) !!}
                                            {!! Form::submit('Eliminar',['class' => 'btn btn-outline']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>

            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#nuevo-ticket"><i
                        class="fa fa-plus"></i> Nuevo</a>

            <div class="modal fade in" id="nuevo-ticket">
                <div class="modal-dialog modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Nuevo ticket</h4>
                    </div>
                    {!! Form::open((['route' => 'ticket::store', 'method' => 'POST'])) !!}
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('date','Fecha',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::date('date',\Carbon\Carbon::now(),['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('hour','Hora',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::time('hour',\Carbon\Carbon::now(),['class' => 'form-control', 'required', ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('enrollment','Pantente',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('enrollment_id',$enrollments->pluck('enrollment', 'id'), null,['class' => 'form-control', 'required', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('product','Producto',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('product_id',$products->pluck('product', 'id'), null,['class' => 'form-control', 'required', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('operator','Operador',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('operator_id',$operators->pluck('name', 'id'), null,['class' => 'form-control', 'required', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('axis1','Eje 1',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::number('axis1',null,['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('axis2','Eje 2',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::number('axis2',null,['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('axis3','Eje 3',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::number('axis3',null,['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('axis4','Eje 4',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::number('axis4',null,['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('axis5','Eje 5',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::number('axis5',null,['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar
                        </button>
                        {!! Form::submit('Agregar',['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <a type="button" class="btn btn-success" data-toggle="modal" data-target="#reporte"><i
                        class="fa fa-file"></i> Reporte</a>

            <div class="modal fade in" id="reporte">
                <div class="modal-dialog modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Buscar</h4>
                    </div>
                    {!! Form::open((['route' => 'ticket::report', 'method' => 'GET'])) !!}
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('from','Desde',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::date('from',null,['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('to','Hasta',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::date('to',null,['class' => 'form-control', ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('enrollment','Pantente',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('enrollment_id',$enrollments->pluck('enrollment', 'id'), null,['class' => 'form-control', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('product','Producto',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::select('product_id',$products->pluck('product', 'id'), null,['class' => 'form-control', 'placeholder'=>'Seleccione opción']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar
                        </button>
                        {!! Form::submit('Buscar',['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                dom: 'Bfrtip',
                language: {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'
                },
                'order': [[0, 'asc']] // Order by Date
            });
        });
    </script>
@stop