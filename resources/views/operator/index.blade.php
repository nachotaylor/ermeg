@extends('layout')

@section('content')
    <div class="table-responsive">
        <div class="col-xs-12 row">
            <div class="box-header">
                <h1 class="box-title">Operadores</h1>
            </div>

            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i>Éxito!</h4>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i>Error!</h4>
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="box with-border">
                <div class="box-body" style="overflow-x: auto">
                    <table id="table" class="table table-condensed table-hover dataTable" role="grid"
                           aria-describedby="example1_info">
                        <thead>
                        <tr>
                            <th>Operador</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($operators as $operator)
                            <tr>
                                <td>{{$operator->name}}</td>
                                <td>
                                    <a class="btn btn-primary" data-toggle="modal"
                                       data-target="#edit{{ $operator->id }}"><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modal-danger{{ $operator->id }}"><i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                            </tr>

                            <div class="modal fade in" id="edit{{ $operator->id }}">
                                <div class="modal-dialog modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title">Editar operador</h4>
                                    </div>
                                    {!! Form::model($operator, ['route' => ['operator::update', $operator], 'method' => 'PUT']) !!}
                                    <div class="modal-body box-body">
                                        <div class="form-group">
                                            {!! Form::label('name','Nombre',['class' => 'col-sm-2 control-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::text('name',null,['class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left"
                                                data-dismiss="modal">Cerrar
                                        </button>
                                        {!! Form::submit('Actualizar',['class' => 'btn btn-primary']) !!}
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="modal modal-danger fade" id="modal-danger{{ $operator->id }}"
                                 style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">Delete</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Desea eliminar {{ $operator->name }}?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left"
                                                    data-dismiss="modal">Cerrar
                                            </button>
                                            {!! Form::model($operator, ['route' => ['operator::delete', $operator], 'method' => 'DELETE']) !!}
                                            {!! Form::submit('Eliminar',['class' => 'btn btn-outline']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>

            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#nuevo-operator"><i
                        class="fa fa-plus"></i> Nuevo</a>

            <div class="modal fade in" id="nuevo-operator">
                <div class="modal-dialog modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Nuevo operador</h4>
                    </div>
                    {!! Form::open((['route' => 'operator::store', 'method' => 'POST'])) !!}
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                {!! Form::label('name','Nombre',['class' => 'col-sm-2 control-label']) !!}
                                <div class="col-sm-10">
                                    {!! Form::text('name',null,['class' => 'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar
                        </button>
                        {!! Form::submit('Agregar',['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                dom: 'Bfrtip',
                language: {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'
                },
                'order': [[0, 'asc']] // Order by Date
            });
        });
    </script>
@stop