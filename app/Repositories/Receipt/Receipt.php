<?php

namespace App\Repositories\Receipt;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = [
        'image_url',
        'header',
        'footer',
        'printer'
    ];
}
