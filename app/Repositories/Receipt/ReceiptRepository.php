<?php

namespace App\Repositories\Receipt;

use App\Repositories\Base\BaseRepository;
use Illuminate\Support\Facades\Storage;

class ReceiptRepository extends BaseRepository
{
    protected $model;

    public function __construct(Receipt $receipt)
    {
        $this->model = $receipt;
    }

    public function modify(array $data = [], $file)
    {
        $receipt = $this->model->first();

        if (!is_null($file)) {
            if (!empty($receipt->image_url)) {
                Storage::disk('local')->delete(explode("app", $receipt->image_url)[1]);
            }
            $image = str_replace("public/", "", asset('storage/app/')) . "/" . Storage::disk('local')->put("logo", $file);
            $data['image_url'] = str_replace("\\", "/", $image);
        }

        return $receipt->update($data);
    }
}
