<?php

namespace App\Repositories\Ticket;

use App\Repositories\Enrollment\Enrollment;
use App\Repositories\Operator\Operator;
use App\Repositories\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'date',
        'axis1',
        'axis2',
        'axis3',
        'axis4',
        'axis5',
        'axis_total',
        'enrollment_id',
        'product_id',
        'operator_id'
    ];

    public function enrollment()
    {
        return $this->belongsTo(Enrollment::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }
}
