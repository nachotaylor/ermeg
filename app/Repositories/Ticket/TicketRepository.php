<?php

namespace App\Repositories\Ticket;

use App\Repositories\Base\BaseRepository;
use Carbon\Carbon;


class TicketRepository extends BaseRepository
{
    protected $model;

    public function __construct(Ticket $ticket)
    {
        $this->model = $ticket;
    }

    private function hour($date, $hour)
    {
        return $date = $date . " " . $hour;
    }

    private function total($a1, $a2, $a3, $a4, $a5)
    {
        if (is_null($a1) || is_null($a2) || is_null($a3) || is_null($a4) || is_null($a5)) {
            throw new \Exception('Debe completar todos los ejes!', 1);
        }

        return $a1 + $a2 + $a3 + $a4 + $a5;
    }

    public function store(array $data = [])
    {
        $data['axis_total'] = $this->total($data['axis1'], $data['axis2'], $data['axis3'], $data['axis4'], $data['axis5']);

        $data['date'] = $this->hour($data['date'], $data['hour']);

        return $this->model->create($data);
    }

    public function modify($id, array $data = [])
    {
        $ticket = $this->findOrFail($id);

        $data['axis_total'] = $this->total($data['axis1'], $data['axis2'], $data['axis3'], $data['axis4'], $data['axis5']);

        $data['date'] = $this->hour($data['date'], $data['hour']);

        return $ticket->update($data);
    }

    public function del($id)
    {
        $ticket = $this->findOrFail($id);

        return $ticket->delete();
    }

    public function detail()
    {
        return $this->model
            ->select('tickets.*', 'products.product', 'operators.name', 'enrollments.enrollment')
            ->join('products', 'products.id', 'tickets.product_id')->join('operators', 'operators.id', 'tickets.operator_id')->join('enrollments', 'enrollments.id', 'tickets.enrollment_id')
            ->get();
    }

    public function get($id)
    {
        return $this->model
            ->select('tickets.*', 'products.product', 'operators.name', 'enrollments.enrollment')
            ->join('products', 'products.id', 'tickets.product_id')->join('operators', 'operators.id', 'tickets.operator_id')->join('enrollments', 'enrollments.id', 'tickets.enrollment_id')
            ->where('tickets.id', $id)
            ->first();
    }

    public function report($from = null, $to = null, $enrollment = null, $product = null)
    {
        $query = $this->model->select('tickets.*', 'enrollments.enrollment', 'products.product', 'operators.name')->join('enrollments', 'enrollments.id', 'tickets.enrollment_id')->join('products', 'products.id', 'tickets.product_id')->join('operators', 'operators.id', 'tickets.operator_id');

        if (!is_null($from['from'])) {
            $query->whereDate('date', '>=', Carbon::parse($from['from'])->format('Y-m-d'));
        }

        if (!is_null($to['to'])) {
            $query->whereDate('date', '<=', Carbon::parse($to['to'])->format('Y-m-d'));
        }

        if (!is_null($enrollment['enrollment_id'])) {
            $query->where('tickets.enrollment_id', $enrollment['enrollment_id']);
        }

        if (!is_null($product['product_id'])) {
            $query->where('product_id', $product['product_id']);
        }

        return ['tickets' => $query->get(), 'sum' => $query->sum('axis_total')];
    }
}
