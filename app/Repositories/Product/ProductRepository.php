<?php

namespace App\Repositories\Product;

use App\Repositories\Base\BaseRepository;

class ProductRepository extends BaseRepository
{
    protected $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function store(array $data = [])
    {
        return $this->model->create($data);
    }

    public function modify($id, array $data = [])
    {
        $product = $this->findOrFail($id);

        return $product->update($data);
    }

    public function del($id)
    {
        $product = $this->findOrFail($id);

        return $product->delete();
    }
}
