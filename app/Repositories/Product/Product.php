<?php

namespace App\Repositories\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product'
    ];
}
