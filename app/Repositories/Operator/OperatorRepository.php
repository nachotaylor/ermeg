<?php

namespace App\Repositories\Operator;

use App\Repositories\Base\BaseRepository;

class OperatorRepository extends BaseRepository
{
    protected $model;

    public function __construct(Operator $operator)
    {
        $this->model = $operator;
    }

    public function store(array $data = [])
    {
        return $this->model->create($data);
    }

    public function modify($id, array $data = [])
    {
        $operator = $this->findOrFail($id);

        return $operator->update($data);
    }

    public function del($id)
    {
        $operator = $this->findOrFail($id);

        return $operator->delete();
    }
}
