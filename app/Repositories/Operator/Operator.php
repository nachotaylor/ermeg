<?php

namespace App\Repositories\Operator;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $fillable = [
        'name'
    ];
}
