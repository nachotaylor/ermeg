<?php

namespace App\Repositories\Enrollment;

use App\Repositories\Base\BaseRepository;
use Exception;

class EnrollmentRepository extends BaseRepository
{
    protected $model;

    public function __construct(Enrollment $enrollment)
    {
        $this->model = $enrollment;
    }

    private function validate($enrollment)
    {
        if (!is_null($this->model->where('enrollment', $enrollment)->first())) {
            throw new Exception('La patente se encuentra registrada');
        }

        return strtoupper(str_replace(' ', '', $enrollment));
    }

    public function store(array $data = [])
    {
        $data['enrollment'] = $this->validate($data['enrollment']);

        return $this->model->create($data);
    }

    public function modify($id, array $data = [])
    {
        $enrollment = $this->findOrFail($id);

        $data['enrollment'] = $this->validate($enrollment->enrollment);

        return $enrollment->update($data);
    }

    public function del($id)
    {
        $enrollment = $this->findOrFail($id);

        return $enrollment->delete();
    }
}
