<?php

namespace App\Repositories\Enrollment;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    protected $fillable = [
        'enrollment'
    ];
}
