<?php

namespace App\Http\Controllers\Web;

use App\Repositories\Receipt\ReceiptRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class ReceiptController extends Controller
{
    protected $receipt;

    public function __construct(ReceiptRepository $receipt)
    {
        $this->receipt = $receipt;
    }

    public function index()
    {
        return view('receipt.index', ['receipt' => $this->receipt->all()[0]]);
    }

    public function update(Request $request)
    {
        try {
            $this->receipt->modify($request->all(), $request->file('image_url'));
            return redirect()->back();
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
