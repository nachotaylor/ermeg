<?php

namespace App\Http\Controllers\Web;

use App\Repositories\Enrollment\EnrollmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class EnrollmentController extends Controller
{
    protected $enrollment;

    public function __construct(EnrollmentRepository $enrollment)
    {
        $this->enrollment = $enrollment;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('enrollment.index', ['enrollments' => $this->enrollment->all()]);
    }

    public function store(Request $request)
    {
        try {
            $this->enrollment->store($request->all());
            return redirect()->back()->with('message', 'La patente fue creada.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->enrollment->modify($id, $request->all());
            return redirect()->back()->with('message', 'Patente modificada');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->enrollment->del($id);
            return redirect()->back()->with('message', 'La patente fue eliminada.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
