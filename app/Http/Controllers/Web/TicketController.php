<?php

namespace App\Http\Controllers\Web;

use App\Repositories\Receipt\ReceiptRepository;
use App\Repositories\Ticket\TicketRepository;
use App\Repositories\Enrollment\EnrollmentRepository;
use App\Repositories\Operator\OperatorRepository;
use App\Repositories\Product\ProductRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

class TicketController extends Controller
{
    protected $ticket;
    protected $enrollment;
    protected $product;
    protected $operator;
    protected $receipt;

    public function __construct(TicketRepository $ticket, EnrollmentRepository $enrollment, ProductRepository $product, OperatorRepository $operator, ReceiptRepository $receipt)
    {
        $this->middleware('auth');
        $this->ticket = $ticket;
        $this->enrollment = $enrollment;
        $this->product = $product;
        $this->operator = $operator;
        $this->receipt = $receipt;
    }

    private function receipt($id, $device)
    {
        try {
            $receipt = $this->receipt->all()[0];
            $body = $this->ticket->get($id);
            $connector = new FilePrintConnector($device);
            //$connector = new WindowsPrintConnector( $receipt->printer);
            $printer = new Printer($connector);
            #logo
            if ($receipt->image_url) {
                $printer->setJustification(Printer::JUSTIFY_CENTER);
                $logo = EscposImage::load(str_replace("\\", "/", storage_path()) . explode("storage", $receipt->image_url)[1], false);
                $printer->bitImageColumnFormat($logo);
                $printer->feed();
            }
            #header
            $printer->setEmphasis(true);
            $printer->text($receipt->header . "\n");
            $printer->setEmphasis(false);
            #body
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->text("\nTICKET NRO: " . $body->id . "\n");
            $printer->text("\nFECHA: " . Carbon::parse($body->date)->format('d M y') . "  HORA: " . Carbon::parse($body->date)->format('H:i') . "\n");
            $printer->text("\nEJE              PESO KG\n");
            $printer->text(" 1                 " . $body->axis1 . "\n");
            $printer->text(" 2                 " . $body->axis2 . "\n");
            $printer->text(" 3                 " . $body->axis3 . "\n");
            $printer->text(" 4                 " . $body->axis4 . "\n");
            $printer->text(" 5                 " . $body->axis5 . "\n");
            $printer->text(" ========================\n");
            $printer->text(" TOTAL (KG)        " . $body->axis_total . "\n\n");
            $printer->text("PATENTE: " . $body->enrollment . "\n");
            $printer->text("PRODUCTO: " . $body->product . "\n");
            $printer->text("OPERADOR: " . $body->name . "\n");
            #footer
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text($receipt->footer . "\n");
            $printer->feed(4);
            $printer->cut();
            $printer->close();
        } catch (Exception $e) {
            echo "No se pudo imprimir el Ticket: " . $e->getMessage() . "\n";
        }
    }

    public function index()
    {
        return view('ticket.index', ['tickets' => $this->ticket->detail(), 'enrollments' => $this->enrollment->all(), 'products' => $this->product->all(), 'operators' => $this->operator->all()]);
    }

    public function reprint($id)
    {
        $device = ["/dev/usb/lp1", "/dev/usb/lp0", "/dev/usb/lp2", "/dev/usb/lp3", "/dev/usb/lp4", "/dev/ttyUSB0", "/dev/ttyS0", "/dev/lp0", "/dev/lp1"];
        foreach ($device as $d) {
            $this->receipt($id, $d);
        }

        return redirect()->back();
    }

    public function store(Request $request)
    {
        try {
            $receipt = $this->ticket->store($request->all());
            $this->receipt($receipt->id);
            return redirect()->back()->with('message', 'El ticket fue creado.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->ticket->modify($id, $request->all());
            return redirect()->back()->with('message', 'Ticket modificado');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->ticket->del($id);
            return redirect()->back()->with('message', 'El ticket fue eliminado.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function report(Request $request)
    {
        return view('ticket.report', ['tickets' => $this->ticket->report($request->only('from'), $request->only('to'), $request->only('enrollment_id'), $request->only('product_id')), 'enrollments' => $this->enrollment->all(), 'products' => $this->product->all(), 'operators' => $this->operator->all()]);
    }
}