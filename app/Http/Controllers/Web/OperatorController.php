<?php

namespace App\Http\Controllers\Web;

use App\Repositories\Operator\OperatorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class OperatorController extends Controller
{
    protected $operator;

    public function __construct(OperatorRepository $operator)
    {
        $this->operator = $operator;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('operator.index', ['operators' => $this->operator->all()]);
    }

    public function store(Request $request)
    {
        try {
            $this->operator->store($request->all());
            return redirect()->back()->with('message', 'El operador fue creado.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $this->operator->modify($id, $request->all());
            return redirect()->back()->with('message', 'Operador modificado');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->operator->del($id);
            return redirect()->back()->with('message', 'El operador fue eliminado.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
