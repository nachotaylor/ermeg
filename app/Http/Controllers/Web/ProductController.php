<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\Product\ProductStoreRequest;
use App\Repositories\Product\ProductRepository;
use App\Http\Controllers\Controller;
use Exception;

class ProductController extends Controller
{
    protected $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('product.index', ['products' => $this->product->all()]);
    }

    public function store(ProductStoreRequest $request)
    {
        try {
            $this->product->store($request->all());
            return redirect()->back()->with('message', 'El producto fue creado.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function update(ProductStoreRequest $request, $id)
    {
        try {
            $this->product->modify($id, $request->all());
            return redirect()->back()->with('message', 'Producto modificado');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->product->del($id);
            return redirect()->back()->with('message', 'El producto fue eliminado.');
        } catch (Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}