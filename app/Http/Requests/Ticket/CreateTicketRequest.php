<?php

namespace App\Http\Requests\Ticket;

use Illuminate\Foundation\Http\FormRequest;

class CreateTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'axis1' => 'required|integer',
            'axis2' => 'required|integer',
            'axis3' => 'required|integer',
            'axis4' => 'required|integer',
            'axis5' => 'required|integer',
            'enrollment_id' => 'required|exists:enrollments,id',
            'product_id' => 'required|exists:products,id',
            'operator_id' => 'required|exists:operators,id'
        ];
    }
}
